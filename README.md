# Projet Docker - Serveur Plex

### Sommaire
### I. Présentation du projet 
### II. Pourquoi avoir fait ce choix ?
### III. Présentation de l'architecture du projet
### IV. Difficultés rencontrées
<br/><br/>

## I. Présentation du projet 
L'objectif est de pouvoir déployer automatiquement via Docker, un serveur de streaming  musique et vidéo, accompagné de divers services tel que des gestionnaires de séries et films, d'indexeurs, de téléchargement, ainsi que d'une base de données, le tout avec des données persistantes, afin de pouvoir télécharger et déployer les films et séries de manière automatique vers le serveur de streaming.
Il est également question de mettre en place un accès VPN au service, de manière à pouvoir partager le contenus multimédia avec de la famille ou des amis.

## II. Pourquoi avoir fait ce choix ?
Si j'ai choisis ce projet, c'est avant tout pour pouvoir **gérer** les films et séries que je souhaite visionner, qui ne sont pas disponibles sur les plateformes de streaming auxquelles j'ai accès. Cela a pour but de m'éviter d'avoir à chercher sur une infinité de sources toutes plus dangeureuses les unes que les autres. Les services déployés le feront pour moi, en récupérant les contenus depuis des sources "fiables".


## III. Présentation de l'architecture du projet
Le projet est constitué d'un total de 8 conteneurs, orchestrés par un docker-compose. Un Dockerfile a été spécialement créé pour la mise en place du serveur PHP/Apache.

**Plex :**
```
"Plex est un logiciel client-serveur de gestion multimédia qui permet d'accéder à 
des films, séries, musiques et photos sur le serveur peu importe où le client se 
situe, s'il a une connexion Internet."
source: https://fr.wikipedia.org/wiki/Plex_(logiciel)
```

**MySQL :**
```
Utilisé pour mettre en place la base de données et utilisateur Plex.
```



**Apache/PHP :**
```
Ce service permet de mettre en place un serveur web, il est 
utilisé afin d'établir une connexion entre la base de données 
et une page de connexion et inscription.
Cette page de connexion sert à camoufler l'accès au serveur plex.
```

**PHPMyAdmin :**
```
Gestionnaire de bases de données.
```

**Deluge :**
```
Permet de gerér les téléchargements et de surveiller en détails 
leur état d'avancement et status. 
```

**Sonarr :**
```
Gestionnaire de séries.
Permet de créer et gérer une liste de séries, et de télécharger les épisodes pour chaque saison disponible.
```

**Radarr :**
```
Gestionnaire de films.
C'est le même principe que pour Sonarr.
```

**Jackett :**
```
Pour que les services Sonarr et Radarr puissent avoir accès aux séries et films, et télécharger le contenu, ils doivent pouvoir accéder à des "indexeurs". Les indexeurs sont des sites publiques ou privés de torrents, dispensé aux deux services par Jackett, qui est le gestionnaire d'indexeurs.
```

Divers volumes sont automatiquement montés entre la machine et les containers afin d'avoir des données persistantes, notamment pour : 
- La base de données
La base est créée avec un compte par défaut.

- Le service Jackett
Cela évite de devoir recréer la liste des indexeurs à chaque fois que le projet est déployé.

### Schéma de l'architecture du projet 
![](https://i.imgur.com/uxIvxXL.png)

Pour résumer, depuis Jackett on se lie à plusieurs Indexeurs pour avec accès à des sources de torrents. Depuis Sonarr/Radarr, on sélectionne le contenu souhaité et on lance le téléchargement. Déluge va nous permettre d'accéder au téléchargement et va le placer dans un répertoire "download". Une fois le téléchargement terminé, un dossier comportant le nom du contenu (nom de film/ série) est créé et le contenu y est automatiquement placé.
Le serveur plex va récupérer le contenu pour enfin l'afficher sur l'interface web pour que l'on puisse le visionner.

Le projet est monté en local mais peut être accessible via VPN. 
J'utilise pour cela ZeroTierOne, ce qui me permet de pouvoir faire fonctionner le service chez moi, et d'y accéder depuis l'extérieur que ce soit sur un équipement mobile ou fixe.

### IV. Difficultés rencontrées
J'ai pris un peu de temps pour repérer les dossiers à sélectionner pour monter mes volumes en explorant les containeurs.
Le Dockerfile pour le service Apache/php m'a posé quelques difficultés, tant au niveau de l'installation des module php qu'à niveau du démarrage du container qui se stoppait juste après avoir démarré...
J'ai également eu des problèmes de droits sur mon serveur plex pour l'accès aux dossiers des films et séries mais j'ai réglé mon soucis en recréant simplement mon container.
La configuration des services (depuis leur interface) a été particulièrement compliquée du fait de mon manque de connaissance et du peu de documentation intéressante que j'ai pu trouver. C'est à force de tester et réessayer que j'ai pu avancer (je fais référence à la connexion entre Deluge et Sonarr/Raddarr, et l'ajout des indexeurs)
